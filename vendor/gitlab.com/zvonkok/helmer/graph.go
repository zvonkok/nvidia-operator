package helmer

import (
	"github.com/gohobby/deepcopy"
)

// DeepCopyInto copy an object into another
func (in *Graph) DeepCopyInto(out *Graph) {
	*out = *in
	out.RepoEntry = in.RepoEntry
	out.ChartSpec = in.ChartSpec
	// out.ChartValues = in.ChartValues
	out.ChartValues = deepcopy.Map(in.ChartValues).Clone()
	out.ReleaseName = in.ReleaseName
}
