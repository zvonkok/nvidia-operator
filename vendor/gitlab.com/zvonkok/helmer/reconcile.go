package helmer

import (
	"context"
	"flag"
	"os"

	"k8s.io/klog/v2"
	klog "k8s.io/klog/v2"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func init() {
	flags := flag.FlagSet{
		Usage: func() {
		},
	}
	// Default is logtostderr
	klog.InitFlags(&flags)
	os.Setenv("HELMER_DEBUG", "1")
}

// Reconcile a Graph with Helmer
func Reconcile(ctx context.Context, req ctrl.Request, clt client.Client) (ctrl.Result, error) {

	klog.Infof("Reconcile %v", req)
	return ctrl.Result{}, nil
}

/* NewInClusterClient returns a new Kubernetes client that expect to run inside the cluster
func NewInClusterClient() (Client, error) {
	config, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return &clientImpl{
		clientset: clientset,
	}, nil
}
*/
