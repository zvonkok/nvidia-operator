package helmer

import (
	helmclient "github.com/mittwald/go-helm-client"
	"helm.sh/helm/v3/pkg/chartutil"
)

// Type Guard asserting that Helmer satisfies the Helmer interface.
var _ Interface = &Helmer{}
var _ GraphInterface = &Graph{}

// Helmer describes the resource to be built
type Helmer struct {
	Graph       Graph                         `json:"graph"`
	HelmClient  helmclient.Client             `json:"helmClient"`
	HelmOptions helmclient.GenericHelmOptions `json:helmOptions"`
	Debug       bool                          `json:"debug"`
}

// Graph encapsulate a chart, repo, values and its dependencies
type Graph struct {
	RepoEntry HelmRepoEntry        `json:"repo"`
	ChartSpec helmclient.ChartSpec `json:"chart"`
	// +kubebuilder:validation:Schemaless
	// +kubebuilder:pruning:PreserveUnknownFields
	// +kubebuilder:validation:Optional
	//ChartValues unstructured.Unstructured `json:"values,omitempty"`
	ChartValues chartutil.Values `json:"values,omitempty"`
	// +kubebuilder:validation:Optional
	ReleaseName string `json:"releaseName,omitempty"`
}

// HelmRepoEntry is a replica of Helms repo.Entry with kubebuilder markers
type HelmRepoEntry struct {
	Name string `json:"name"`
	URL  string `json:"url"`
	// +kubebuilder:validation:Optional
	Username string `json:"username"`
	// +kubebuilder:validation:Optional
	Password string `json:"password"`
	// +kubebuilder:validation:Optional
	CertFile string `json:"certFile"`
	// +kubebuilder:validation:Optional
	KeyFile string `json:"keyFile"`
	// +kubebuilder:validation:Optional
	CAFile string `json:"caFile"`
	// +kubebuilder:validation:Optional
	InsecureSkipTLSverify bool `json:"insecure_skip_tls_verify"`
	// +kubebuilder:validation:Optional
	PassCredentialsAll bool `json:"pass_credentials_all"`
}
