package helmer

// Interface a helm hepler
type Interface interface {
	InstallOrUpgradeGraph() error
	Upgrade() error
	Lint() error
	Template() error
	AddOrUpdateRepo() error
	GetHelmClientWithKubeConf(path string, kubeContext string) error
}

// GraphInterface a simple interface for Graph
type GraphInterface interface {
	DeepCopyInto(*Graph)
}
