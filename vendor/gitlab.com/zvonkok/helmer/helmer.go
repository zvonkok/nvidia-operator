package helmer

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"reflect"

	helmclient "github.com/mittwald/go-helm-client"
	"sigs.k8s.io/yaml"

	"github.com/pkg/errors"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chartutil"
	"helm.sh/helm/v3/pkg/repo"
	"k8s.io/klog/v2"
)

// GetHelmClientWithKubeConf create a Helmer with a supplied KubeConf
func (h *Helmer) GetHelmClientWithKubeConf(path string, kubeContext string) error {

	if path == "" {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return errors.Wrapf(err, "\n[GetClientWithKubeConf]\tcannot read user home dir")
		}
		path = homeDir + "/.kube/config"
	}

	kubeConfig, err := os.ReadFile(path)
	if err != nil {
		return errors.Wrapf(err, "\n[GetClientWithKubeConf]\tcannot read kubeConfig from path %s:%v", path, err)
	}

	opt := &helmclient.KubeConfClientOptions{
		Options: &helmclient.Options{
			Namespace:        h.Graph.ChartSpec.Namespace, // Change this to the namespace you wish to install the chart in.
			RepositoryCache:  "/tmp/.helmcache",
			RepositoryConfig: "/tmp/.helmrepo",
			Debug:            true,
			Linting:          false, // Change this to false if you don't want linting.
			DebugLog:         klog.Infof,
		},
		KubeContext: kubeContext,
		KubeConfig:  kubeConfig,
	}

	h.HelmClient, err = helmclient.NewClientFromKubeConf(opt)
	if err != nil {
		return errors.Wrap(err, "\n[GetClientWithKubeConf]\tcannot create client from kubeConfig")
	}
	return nil
}

// New creates a simple Helmer object with debugging flag set
func New() (*Helmer, error) {

	h := &Helmer{
		HelmOptions: helmclient.GenericHelmOptions{
			PostRenderer: nil,
			RollBack:     nil,
		},
		Debug: false,
	}

	os.Setenv("HELM_DEBUG", "1")
	os.Setenv("HELM_PLUGINS", "/tmp/.helmplugins")

	if debug := os.Getenv("HELMER_DEBUG"); debug == "1" {
		h.Debug = true
	}
	return h, nil
}

// NewWithGraph creates a very simple Helmer object
func NewWithGraph(graph *Graph) (*Helmer, error) {

	h, _ := New()
	h.Graph = *graph
	h.Graph.ReleaseName = h.Graph.ChartSpec.ReleaseName

	return h, nil
}

// LoadGraph loads a graph from various sources
func LoadGraph(object interface{}) (*Graph, error) {

	var err error
	var g *Graph

	switch t := object.(type) {
	case string:

		klog.Info("Graph:", object)
		if g, err = LoadGraphFromFile(t); err != nil {
			return nil, errors.Wrapf(err, "\n[Load]\tfailed loading Graph from file: %s", t)
		}
		return g, nil

	case map[string]interface{}:

		klog.Info("Graph:", object)
		if g, err = LoadGraphFromObject(t); err != nil {
			return nil, errors.Wrapf(err, "\n[Load]\tfailed loading Graph from map: %v", t)
		}

		return g, nil

	default:
		return nil, errors.New("\n[Load]\tcannot construct Graph from type: " + reflect.TypeOf(object).String())
	}
}

// LoadGraphFromFile reads a Graph object from provided YANL file
func LoadGraphFromFile(file string) (*Graph, error) {

	graph := &Graph{
		RepoEntry:   repo.Entry{},
		ChartSpec:   helmclient.ChartSpec{},
		ChartValues: map[string]interface{}{},
	}

	yamlText, err := os.ReadFile(file)
	if err != nil {
		return nil, errors.Wrapf(err, "\n[LoadGraphFromFile]\tcannot read %s from path %s:%v", yamlText, file, err)
	}

	jsonText, err := yaml.YAMLToJSON(yamlText)
	if err != nil {
		return nil, errors.Wrapf(err, "\n[LoadGraphFromFile]\tfailed on %s", yamlText)
	}
	dec := json.NewDecoder(bytes.NewReader(jsonText))
	dec.DisallowUnknownFields()

	if err := dec.Decode(graph); err != nil {
		return nil, errors.Wrapf(err, "\n[LoadGraphFromFile]\tfailed on %s", jsonText)
	}

	return graph, err
}

// LoadGraphFromObject loads a Graph from a CR or any other YAML like object
func LoadGraphFromObject(object map[string]interface{}) (*Graph, error) {
	return nil, nil
}

// InstallOrUpgradeGraph implements HelmHelper
func (h *Helmer) InstallOrUpgradeGraph() error {

	// The graph chart values can override chart.Values
	rootValues := h.Graph.ChartValues
	rootChart, _, err := h.HelmClient.GetChart(&h.Graph.ChartSpec)
	if err != nil {
		return errors.Wrapf(err, "\n[InstallOrUpgradeGraph]\tcannot get Chart from Graph %s", h.Graph.ChartSpec.ReleaseName)
	}

	err = h.install(rootChart, &rootValues)
	if err != nil {
		return errors.Wrapf(err, "\n[InstallOrUpgradeGraph]\tcannot install Chart from Graph %s", h.Graph.ChartSpec.ReleaseName)
	}

	return nil
}

func (h *Helmer) install(rootChart *chart.Chart, rootValues *chartutil.Values) error {

	err := chartutil.ProcessDependencies(rootChart, *rootValues)
	if err != nil {
		return errors.Wrapf(err, "[Install] process dependencies failed for %v", rootChart.Name())
	}

	// TODO: Sharing Templates with Subcharts
	// Parent charts and subcharts can share templates.
	// Any defined block in any chart is available to other charts.
	//for _, childChart := range rootChart {
	//	id := childChart.ChartFullPath()
	//}

	// rootValues will hold the value overrides for the child chart
	*rootValues, err = chartutil.CoalesceValues(rootChart, rootValues.AsMap())
	if err != nil {
		return errors.Wrapf(err, "[Install] coalesce values failed %v", rootChart.Name())
	}

	// We need the initial releaseName since we're updating each child chart
	// with a new releaseName, this way we are not concat relase + child0 + child1
	h.installDependencies(rootChart.Dependencies(), rootValues)

	// Reset the releasename if we are the original root chart
	if rootChart.IsRoot() {
		h.Graph.ChartSpec.ReleaseName = h.Graph.ReleaseName
	}
	// Need to reset the root flag, helm aggregates all Values and templates
	// if it is a root chart it will only populate the child values with
	// the root .Value.childChart not the actual child values
	rootChart.NotRoot()

	klog.Info("[InstallRootChart]: ", rootChart.Name())

	if _, err := h.HelmClient.InstallOrUpgradeChart(context.TODO(), &h.Graph.ChartSpec, &h.HelmOptions, rootChart); err != nil {
		fmt.Println("ERROR INSTALLORUPGRADECXHART")
		return errors.Wrapf(err, "\n[Install]\tchart failed with %v", rootChart.Name())
	}

	return nil
}

func (h *Helmer) installDependencies(rootChart []*chart.Chart, rootValues *chartutil.Values) error {

	childValues := chartutil.Values{}

	for _, childChart := range rootChart {

		// Overriding Values from a Parent Values
		// The value at the top level can override the
		// value of the subchart.
		if rootOverride, err := rootValues.Table(childChart.Name()); err == nil {
			childValues = chartutil.CoalesceTables(childValues, rootOverride)
		}

		// Global Chart Values
		// Global values are values that can be accessed from any
		// chart or subchart by exactly the same name.
		if rootGlobal, err := rootValues.Table("global"); err == nil {
			childGlobal, err := childValues.Table("global")
			if err != nil {
				return errors.Wrap(err, "\n[installDependencies]\t cannot extract global from childValues")
			}
			childValues["global"] = chartutil.CoalesceTables(childGlobal, rootGlobal)
		}

		h.installDependency(childChart, &childValues)
	}
	return nil
}

func (h *Helmer) updateChildGraph(childChart *chart.Chart) {
	h.Graph.ChartSpec.ReleaseName = h.Graph.ReleaseName + "-" + childChart.Name()
	h.Graph.ChartSpec.ChartName = h.Graph.RepoEntry.Name + "/" + childChart.Name()
	h.Graph.ChartSpec.Version = childChart.Metadata.Version
}

func (h *Helmer) installDependency(childChart *chart.Chart, childValues *chartutil.Values) error {

	// For each chart we create an Helmer instance with its own settings
	// this makes it easier to decouple each chart for processing and clients
	// that do not interfere with each other.

	// Copy root definitions and apply to child charts, we may think of
	// own graph definitions for child charts
	h.updateChildGraph(childChart)

	klog.Info(h.Graph.ChartSpec)

	c, err := NewWithGraph(&h.Graph)
	if err != nil {
		return errors.Wrapf(err, "\n[installDependency]\tcannot create new Helmer with Graph %s", h.Graph.ChartSpec.ReleaseName)
	}

	// TODO: add generic client which can handle "all" situations
	err = c.GetHelmClientWithKubeConf("", "default")
	if err != nil {
		return errors.Wrapf(err, "\n[installDependency]\tcannot get client with kubeConf")
	}

	klog.Info("[InstallChildChart]: ", childChart.Name())

	err = c.install(childChart, childValues)
	if err != nil {
		return errors.Wrapf(err, "\n[installDependency]\tcannot install chart: %s", childChart.Name())
	}
	return nil
}

// Upgrade implements HelmHelper
func (h *Helmer) Upgrade() error {
	panic("unimplemented")
}

// Lint implement HelmHelper
func (h *Helmer) Lint() error {
	if err := h.HelmClient.LintChart(&h.Graph.ChartSpec); err != nil {
		return errors.Wrap(err, "[Lint] failed linting chart")
	}
	return nil
}

// Template implement HelmHelper
func (h *Helmer) Template() error {
	var err error
	yamls := []byte{}

	if yamls, err = h.HelmClient.TemplateChart(&h.Graph.ChartSpec, nil); err != nil {
		return errors.Wrap(err, "[Template] templating failed")
	}
	if h.Debug {
		klog.Info(string(yamls))
	}

	return nil
}

// AddOrUpdateRepo implements HelmHelper
func (h *Helmer) AddOrUpdateRepo() error {

	if err := h.HelmClient.AddOrUpdateChartRepo(h.Graph.RepoEntry); err != nil {
		return errors.Wrapf(err, "[AddOrUpdateChartRepo] failed with repo entry %v", h.Graph.RepoEntry)
	}

	return nil
}
